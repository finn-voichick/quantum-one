# QuantumOne Language

A quantum programming language that does not distinguish between classical and
quantum data.

Running the `make` command should compile most of the files.

## Files

- src/QuantumOne.v -
  the definition of the syntax of the language
- src/Vector.v -
  definitions of vectors
- src/Semantics.v -
  the operational semantics of the language in terms of a step relation
- src/Types.v -
  type definitions and a typing relation
- src/VectorTypes.v -
  useful functions and definitions relating to both semantics and types
- src/Lib.v -
  a few generally useful "library" functions
- src/Compiler.v -
  a compiler from QuantumOne to SQIR
- src/CompilerExample.v -
  work toward proving an example compilation correct
- src/examples/CoinFlip.v -
  an example program that simply flips a coin
- src/examples/Deutch.v -
  Deutch's Algorithm
- src/examples/DeutchJozsa.v -
  The Deutch-Jozsa Algorithm

