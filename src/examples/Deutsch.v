(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import String Reals.
Require Import Maps.
Require Import QuantumOne Types Lib.

Definition deutsch u :=
  <{ hadamard ({fst <{{Bit}}> <{{Bit}}>} (u <plus, minus>)) }>.

Lemma has_type_deutsch :
  forall u,
  |> u : (Bit * Bit -> Bit * Bit) ->
  |- {deutsch u} : Bit.
Proof. eauto with type_db. Qed.
