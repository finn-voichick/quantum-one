(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import String Reals.
Require Import Maps.
Require Import QuantumOne Types Lib.

Definition all_zeros n := all <{{ Bit }}> not n.

Lemma has_type_all_zeros :
  forall n,
  |> {all_zeros n} : (Reg[n] -> Bit).
Proof. eauto with type_db. Qed.

Definition deutsch_jozsa n u :=
  <{ not
       ({all_zeros n}
          ({fst <{{Reg[n]}}> <{{Bit}}>}
             (u < {multi_hadamard n} ({replicate n <{{Bit}}>} 0),
                  minus>))) }>.

Lemma has_type_deutsch_jozsa :
  forall n u,
  |> u : (Reg[n] * Bit -> Reg[n] * Bit) ->
  |- {deutsch_jozsa n u} : Bit.
Proof. eauto 9 with type_db. Qed.
