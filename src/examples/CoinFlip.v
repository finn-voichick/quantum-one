(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import String List Reals.
Require Import Complex.
Require Import Maps.
Require Import QuantumOne src.Vector Semantics src.Types VectorTypes Lib.
Import ListNotations.
Import VarNames.

Open Scope R_scope.
Open Scope C_scope.

(* "meas" and "plus" are defined in src/Lib.v. *)
Definition coin_flip := <{ {meas <{{Bit}}>} plus }>.

Lemma has_type_coin_flip :
  |- coin_flip : Bit.
Proof. type_check. Qed.

(* A coin flip produces a maximally mixed bit. *)
Lemma coin_flip_maximally_mixed :
  coin_flip =[ maximally_mixed <{{ Bit }}> ].
Proof.
  exists (scale (/ sqrt 2)
                (vector_sum (ket (<{ 0 }>, <{ 0 }> :: []))
                            (ket (<{ 1 }>, <{ 1 }> :: [])))).
  simpl. repeat split.
  - repeat constructor.
  - econstructor;
    try (replace coin_flip with (plug (ctx_app (meas <{{ Bit }}>) ctx_hole)
                                      <{ hadamard 0 }>)
                           by reflexivity;
         repeat constructor).
    simpl. rewrite Cexp_0, Cmult_1_r. repeat rewrite Cmult_1_l.
    replace (PI / 2 / 2)%R with (PI / 4)%R by lra. rewrite sin_cos_PI4, cos_PI4.
    replace (1 / sqrt 2)%R with (/ sqrt 2)%R by lra.
    econstructor.
    + constructor.
      replace <{ {meas <{{ Bit }}>} 0 }> with (plug ctx_hole
                                                    <{ {meas <{{ Bit }}>} 0 }>)
                                         by reflexivity.
      repeat constructor. discriminate.
    + simpl. econstructor.
      * constructor.
        replace <{ (\y : Bit, 0) 0 }> with (plug ctx_hole <{ (\y : Bit, 0) 0 }>)
                               by reflexivity.
        constructor. apply cstep_app_abs2; repeat constructor.
        intros H. inversion H. inversion H1.
      * simpl. econstructor.
        -- apply step_sum1; repeat constructor.
           replace <{ {meas <{{Bit}}>} 1 }> with (plug ctx_hole
                                                  <{ {meas <{{Bit}}>} 1 }>)
                                by reflexivity.
           repeat constructor. discriminate.
        -- simpl. econstructor.
           ++ apply step_sum1.
              ** repeat constructor.
              ** replace <{ (\y : Bit, 1) 1 }> with (plug ctx_hole
                                                          <{ (\y : Bit, 1) 1 }>)
                                        by reflexivity.
                 constructor. apply cstep_app_abs2; repeat constructor.
                 intros H. inversion H. inversion H1.
           ++ simpl. repeat rewrite Cmult_1_r.
              rewrite RtoC_inv; try reflexivity.
              apply Rgt_not_eq. apply Rlt_gt. apply Rlt_sqrt2_0.
  - apply functional_extensionality. intros x_c.
    apply functional_extensionality. intros x_r.
    assert (sqrt (/ 2) * sqrt (/ 2) = / 2) as H.
    { rewrite <- RtoC_mult, <- RtoC_inv; try lra.
      apply f_equal, sqrt_def. lra. }
    unfold maximally_mixed, collapse. simpl. rewrite Cmult_1_r.
    destruct (expr_dec x_c x_r).
    + subst. destruct (expr_dec x_r <{ 0 }>).
      * subst. simpl.
        rewrite Cplus_0_r, Cplus_0_r, <- Csqrt_inv, Cconj_R; try lra.
        assumption.
      * destruct (expr_dec x_r <{ 1 }>).
        -- subst. simpl.
           rewrite Cplus_0_l, Cplus_0_l, <- Csqrt_inv, Cconj_R; try lra.
           assumption.
        -- destruct (value_in_type_dec x_r <{{ Bit }}>); try lca.
           apply value_in_type_enumerate in v. simpl in v.
           destruct v as [H0 | [H1 | Hf]]; subst; contradiction.
    + destruct (expr_dec x_r <{ 0 }>),
               (expr_dec x_c <{ 0 }>),
               (expr_dec x_c <{ 1 }>),
               (expr_dec x_r <{ 1 }>);
      subst; simpl; try contradiction; try discriminate; try lca.
Qed.

