(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import String List.
Require Import Maps.
Require Import SQIR UnitaryOps DensitySem.
Require Import QuantumOne src.Types Lib.
Import ListNotations.
Import S.

Open Scope com_scope.
Open Scope nat_scope.

(* Number of qubits used to represent a given type *)
Fixpoint size T : nat :=
  match T with 
  | <{{ Unit }}> => 0
  | <{{ T0 + T1 }}> => 1 + max (size T0) (size T1)
  | <{{ T1 * T2 }}> => size T1 + size T2
  end.

(* Maps variable names to the (first) qubit location. *)
Definition ptr_context := partial_map nat.

(* SQIR has no unitary skip, so we have to rely on options. *)
Definition ocom dim := option (base_ucom dim).

Definition oseq {dim} (p1 p2 : ocom dim) : ocom dim :=
  match p1, p2 with
  | _, None => p1
  | None, _ => p2
  | Some p1', Some p2' => Some (useq p1' p2')
  end.

Infix ">>" := oseq (at level 50).

Definition ocom2com {dim} (p : ocom dim) : base_com dim :=
  match p with
  | None => skip
  | Some p' => uc p'
  end.

(* Either unitary or not. This separation is useful for the compiler. *)
Inductive mcom dim : Set :=
  | umcom (p : ocom dim)
  | mmcom (p : base_com dim).

Arguments umcom {dim}.
Arguments mmcom {dim}.

Definition mcom2com {dim} p : base_com dim :=
  match p with
  | umcom p' => ocom2com p'
  | mmcom p' => p'
  end.

(* Sequencing unitaries produces a unitary. *)
Definition mseq {dim} p1 p2 : mcom dim :=
  match p1, p2 with
  | umcom p1', umcom p2' => umcom (p1' >> p2')
  | _, _ => mmcom (mcom2com p1; mcom2com p2)
  end.

Infix ";;" := mseq (at level 50).

Lemma mseq_seq :
  forall dim (p1 p2 : mcom dim),
  c_equiv (mcom2com (p1;; p2)) (mcom2com p1; mcom2com p2).
Proof.
  intros dim p1 p2 Hd rho Hr.
  destruct p1 as [p1 | p1], p2 as [p2 | p2]; try reflexivity.
  destruct p1 as [p1 |], p2 as [p2 |]; try reflexivity.
  simpl. rewrite compose_super_eq. reflexivity.
Qed.

(* Shares the n-qubit register "from" to "to" by performing n CNOT gates. *)
Fixpoint share {dim} n from to : ocom dim :=
  match n with
  | 0 => None
  | S n' => Some (CNOT from to) >> share n' (1 + from) (1 + to)
  end.

(* Swaps only if the two indices are different. *)
Definition swap {dim} from to : ocom dim :=
  if from =? to then None else Some (SWAP from to).

(* Swaps n-qubit registers "from" and "to" by performing n SWAP gates. *)
Fixpoint swap_n {dim} n from to : ocom dim :=
  match n with
  | 0 => None
  | S n' => swap from to >> swap_n n' (1 + from) (1 + to)
  end.

(* Resets n qubits starting at i. *)
Fixpoint reset_n {dim} n i : base_com dim :=
  match n with
  | 0 => skip
  | S n' => reset i; reset_n n' (1 + i)
  end.

(* New notation needed because of conflicts between Maps and SQIR. *)
Notation "x |-> v , m" := (Maps.update m x v)
  (at level 100, v at next level, right associativity).

(* Duplicates variable x to location i, updating the pointer context c. *)
Definition dup_var {dim} (Gamma : context) x (c : ptr_context) i :
  ocom dim * ptr_context * nat :=
  match Gamma x, c x with
  | Some T, Some q =>
      let size_T := size T in
      (share (size_T) q i, (x |-> i, c), i + size_T)
  | _, _ => (None, c, i)
  end.

(* Duplicates a list of variables, returning the next unused qubit. *)
Fixpoint dup_vars {dim} Gamma v c i : ocom dim * ptr_context * nat :=
  match v with
  | [] => (None, c, i)
  | x :: v' =>
      let '(p, new_c, new_i) := dup_var Gamma x c i in
      let '(p', newer_c, newer_i) := dup_vars Gamma v' new_c new_i in
      (p >> p', newer_c, newer_i)
  end.

(* Duplicate the intersection of the two sets. *)
Definition dup_inter {dim} Gamma c v v' i : ocom dim * ptr_context * nat :=
  dup_vars Gamma (elements (inter v v')) c i.

(* Duplicates all variables used by both e and e'. *)
Definition dup_ptrs {dim} Gamma c e e' i : ocom dim * ptr_context * nat :=
  dup_inter Gamma c (free_vars e) (free_vars e') i.

(* Like dup_ptrs, but with a function instead of an expression. *)
Definition dup_ptrs_fn {dim} Gamma c f e i : ocom dim * ptr_context * nat :=
  dup_inter Gamma c (free_vars_fn f) (free_vars e) i.

(* Reset the qubits storing a variable. *)
Definition clear_var {dim} (Gamma : context) (c : ptr_context) x :
  base_com dim :=
  match Gamma x, c x with
  | Some T, Some i => reset_n (size T) i
  | _, _ => skip
  end.

(* Clear all variables in a list. *)
Fixpoint clear_vars {dim} Gamma c v : base_com dim :=
  match v with
  | [] => skip
  | x :: v' => clear_var Gamma c x; clear_vars Gamma c v'
  end.

(* Clear all variables in a set. *)
Definition clear_var_set {dim} Gamma c v : base_com dim :=
  clear_vars Gamma c (elements v).

(* Conversion from our gate set to SQIR's base. *)
Definition gate2unitary g :=
  match g with
  | U_R theta phi lambda => SQIR.U_R theta phi lambda
  end.

(* Normal controlled operation, coherently applies p if i is 1. *)
Definition control1 {dim} i (p : ocom dim) :=
  match p with
  | None => None
  | Some p' => Some (UnitaryOps.control i p')
  end.

(* Like control1, but only applies if i is 0 instead of 1. *)
Definition control0 {dim} i (p : ocom dim) : ocom dim :=
  Some (X i) >> control1 i p >> Some (X i).

(* Applies p0 if i is 0 and p1 if i is 1. *)
Definition qmux {dim} i (p0 p1 : ocom dim) := control0 i p0 >> control1 i p1.

(* No-op is unitary. *)
Definition noop {dim} : mcom dim := umcom None.

(* Compile expression e, producing output to location i. All qubits i and
 * greater are assumed to be initialized to |0>, and the only qubits used by
 * the compiled circuit are i and greater, as well as the location of any free
 * variables (retrieved from pointer context c). If a variable (in c) is used by
 * (free in) e, then the compiled circuit leaves the qubits holding that
 * value in the |0> state. Any qubits after the output are also reset to |0>.
 *)
Fixpoint compile_expr {dim} e Gamma c i : mcom dim :=
  match e with
  | <{ <> }> => noop
  | <{ inl T e' }> => compile_expr e' Gamma c (1 + i)
  | <{ inr T e' }> => umcom (Some (X i));; compile_expr e' Gamma c (1 + i)
  | <{ <e1, e2> }> =>
      match type_check Gamma e1,
            type_check Gamma e2,
            dup_ptrs Gamma c e1 e2 i
      with
      | Some T1, Some T2, (p, new_c, new_i) =>
          let size_T1 := size T1 in
          (* duplicate free variables *)
          umcom p;;
          (* two sub-compilations use different pointer contexts *)
          compile_expr e1 Gamma new_c new_i;;
          (* move result of e1 back to i, where it should be *)
          umcom (swap_n size_T1 new_i i);;
          compile_expr e2 Gamma c (size_T1 + i)
      | _, _, _ => noop
      end
  | expr_var x => match c x, Gamma x with
                  | Some q, Some T => umcom (swap_n (size T) q i)
                  | _, _ => noop
                  end
  | <{ case e' of | inl x0 => e0 | inr x1 => e1 }> => 
      let sub_free_vars :=
        union (remove x0 (free_vars e0)) (remove x1 (free_vars e1))
      in
      match type_check Gamma e,
            type_check Gamma e',
            dup_inter Gamma c (free_vars e') sub_free_vars i
      with
      | Some T, Some T', (p, new_c, new_i) =>
          let size_T' := size T' in
          let result_i := size_T' + i in
          match T' with
          | <{{ T0 + T1 }}> =>
              let i' := 1 + i in
              umcom p;;
              compile_expr e' Gamma new_c new_i;;
              umcom (swap_n size_T' new_i i);;
              mmcom (SQIR.meas i
                               (mcom2com (compile_expr e1
                                                       (x1 |-> T1, Gamma)
                                                       (x1 |-> i', c)
                                                       result_i))
                               (mcom2com (compile_expr e0
                                                       (x0 |-> T0, Gamma)
                                                       (x0 |-> i', c)
                                                       result_i)));;
              (* necessary because subexpressions may consume different vars *)
              mmcom (clear_var_set Gamma c sub_free_vars);;
              mmcom (reset_n size_T' i);;
              umcom (swap_n (size T) new_i i)
          | _ => noop
          end
      | _, _, _ => noop
      end
  | <{ case e' of | 0 => e0 | 1 => e1 }> =>
      let sub_free_vars := union (free_vars e0) (free_vars e1) in
      match type_check Gamma e,
            dup_inter Gamma c (free_vars e') sub_free_vars i
      with
      | None, _ => noop
      | Some T, (p, new_c, new_i) =>
          let i' := 1 + i in
          umcom p;;
          compile_expr e' Gamma new_c new_i;;
          umcom (swap new_i i);;
          mmcom (SQIR.meas i
                           (mcom2com (compile_expr e1 Gamma c i'))
                           (mcom2com (compile_expr e0 Gamma c i')));;
          mmcom (clear_var_set Gamma c sub_free_vars);;
          mmcom (reset i);;
          umcom (swap_n (size T) i' i)
      end
  | <{ control e' with | inl x0 => e0 | inr x1 => e1 }> =>
      let i' := 1 + i in
      match type_check Gamma e',
            type_check Gamma e0,
            type_check Gamma e1,
            dup_inter Gamma c (free_vars e') (remove x0 (free_vars e0)) i
      with
      | Some T, Some T0, Some T1, (p, new_c, new_i) =>
          let size_T := size T in
          let result_i := size_T + i in
          match compile_expr e0 (x0 |-> T0, Gamma) (x0 |-> i', c) result_i,
                compile_expr e1 (x1 |-> T1, Gamma) (x1 |-> i', c) result_i
          with
          | umcom p0, umcom p1 =>
              umcom p;;
              compile_expr e' Gamma new_c new_i;;
              umcom (swap_n size_T new_i i);;
              umcom (qmux i p0 p1)
          | _, _ => noop
          end
      | _, _, _, _ => noop
      end
  | <{ control e' with | 0 => e0 | 1 => e1 }> =>
      let i' := 1 + i in
      match dup_ptrs Gamma c e' e0 i,
            compile_expr e0 Gamma c i',
            compile_expr e1 Gamma c i'
      with
      | (p, new_c, new_i), umcom p0, umcom p1 =>
          umcom p;;
          compile_expr e' Gamma new_c new_i;;
          umcom (swap new_i i);;
          umcom (qmux i p0 p1)
      | _, _, _ => noop
      end
  | <{ let <x1, x2> = e1 in e2 }> =>
      match type_check Gamma e,
            type_check Gamma e1,
            dup_inter Gamma
                      c
                      (free_vars e1)
                      (remove x1 (remove x2 (free_vars e2)))
                      i
      with
      | Some T, Some T', (p, new_c, new_i) =>
          let size_T' := size T' in
          match T' with
          | <{{ T1 * T2 }}> =>
              let size_T1 := size T1 in
              let x2_reg := size_T1 + new_i in
              let result_i := size_T' + i in
              umcom p;;
              compile_expr e1 Gamma new_c new_i;;
              umcom (swap_n (size_T') new_i i);;
              (if appears_free_in_dec x1 e2
               then noop
               else mmcom (reset_n (size_T1) new_i));;
              (if appears_free_in_dec x2 e2
               then noop
               else mmcom (reset_n (size T2) x2_reg));;
              compile_expr e2
                           (x1 |-> T1, x2 |-> T2, Gamma)
                           (x1 |-> new_i, x2 |-> x2_reg, c)
                           result_i;;
              umcom (swap_n (size T) result_i i)
          | _ => noop
          end
      | _, _, _ => noop
      end
  | <{ f e' }> =>
      match type_check Gamma e',
            type_check Gamma e,
            dup_ptrs_fn Gamma c f e' i
      with
      | Some T, Some T', (p, new_c, new_i) => 
          let output := size T + new_i in
          umcom p;;
          compile_expr e' Gamma c new_i;;
          compile_fn f Gamma new_c new_i output;;
          umcom (swap_n (size T') output i)
      | _, _, _ => noop
      end
  end
(* Like compile_expr, but has both input (i) and output (o). *)
with compile_fn {dim} f Gamma c i o : mcom dim :=
  match f with
  | <{ \x : T, e }> =>
    if appears_free_in_dec x e
    then compile_expr e (x |-> T, Gamma) (x |-> i, c) o
    else mmcom (reset_n (size T) i);; compile_expr e Gamma c o
  | fn_gate g => umcom (swap i o >> Some (uapp1 (gate2unitary g) o))
  end.

(* The minimum dimension that this ucom can have. *)
Fixpoint ucom_used_dim {d} (p : base_ucom d) :=
  match p with
  | uapp1 _ i => S i
  | uapp2 _ i1 i2 => S (max i1 i2)
  | uapp3 _ i1 i2 i3 => S (max (max i1 i2) i3)
  | useq p1 p2 => max (ucom_used_dim p1) (ucom_used_dim p2)
  end.

(* The minimum dimension that this com can have. *)
Fixpoint com_used_dim {d} (p : base_com d) :=
  match p with
  | skip => 0
  | uc p' => ucom_used_dim p'
  | p1; p2 => max (com_used_dim p1) (com_used_dim p2)
  | mif i then p1 else p0 => max (S i) (max (com_used_dim p1) (com_used_dim p0))
  end.

(* The minimum dimension that this mcom can have. *)
Definition used_dim {d} (p : mcom d) := com_used_dim (mcom2com p).

(* The dimension that this expression should compile to. *)
Definition calc_dim e := used_dim (@compile_expr 0 e Maps.empty Maps.empty 0).

(* Compile an expression to index 0 with no context and a computed dimension. *)
Definition compile e : mcom (calc_dim e) :=
  compile_expr e Maps.empty Maps.empty 0.

(*
Definition exp9 := <{ hadamard ({fst <{{Bit}}> <{{Bit}}>} <plus, minus>) }>.
Compute calc_dim exp9.
Compute compile exp9.

Definition exp10 := <{ ({fst <{{Bit}}> <{{Bit}}>} <0, 1>) }>.
Compute calc_dim exp10.
Compute compile exp10.

Definition exp := <{ <0, <1, 0> > }>.
Compute calc_dim exp.
Compute compile exp.

Definition exp2 := <{ <1, <0, 1> > }>.
Compute calc_dim exp2.
Compute compile exp2.

Definition exp25 := <{ <<1, 0> , 1>  }>.
Compute calc_dim exp25.
Compute compile exp25.

Local Open Scope string_scope.
Definition exp3 := <{let < "a", "b"> = <1, <0, 1> > in "b" }>.
Compute calc_dim exp3.
Compute compile exp3.

Definition exp35 := <{let < "a", "b"> = <1,  1 > in <"a", "a" > }>.
Compute calc_dim exp35.
Compute compile exp35.

Definition exp4 := <{inr Bit 0 }>.
Compute calc_dim exp4.
Compute compile exp4.

Definition exp5 := <{case 1 of | 0 => <0, 0> | 1 => <1, 1> }>.
Compute calc_dim exp5.
Compute compile exp5.

Definition exp6 := <{let <"a", "b"> = <0, 1>  in "b" }>.
Compute calc_dim exp6.
Compute compile exp6.

Definition exp7 := <{let <"a", "b"> = <0,< 1, 0> >  in case "a" of |0 => <0, 1> |1=> <1, 0> }>.
Compute calc_dim exp7.
Compute compile exp7.

Definition exp75 := <{plus }>.
Compute calc_dim exp75.
Compute compile exp75.

Definition exp8 := <{ hadamard ({fst <{{Bit}}> <{{Bit}}>} <plus, minus>) }>.
Compute calc_dim exp8.
Compute compile exp8.
*)

(* A discard-free expression compiles to a ucom. *)
Lemma discard_free_unitary :
  forall e,
  discard_free e ->
  forall dim Gamma c i,
  exists p : ocom dim, compile_expr e Gamma c i = umcom p
with discard_free_fn_unitary :
  forall f,
  discard_free_fn f ->
  forall dim Gamma c i o,
  exists p : ocom dim, compile_fn f Gamma c i o = umcom p.
Proof.
  - assert (forall dim u, exists (p : ocom dim), umcom u = umcom p) as Hu.
    { intros dim u. exists u. reflexivity. }
    intros e H dim Gamma c i. destruct e; inversion_clear H; simpl.
    + apply Hu.
    + auto using discard_free_unitary.
    + eapply discard_free_unitary in H0 as [p H].
      rewrite H. apply Hu.
    + destruct (type_check Gamma e1) as [T1 |]; try apply Hu.
      destruct (type_check Gamma e2) as [T2 |]; try apply Hu.
      destruct (dup_ptrs Gamma c e1 e2) as [[p new_c] new_i].
      eapply discard_free_unitary in H1 as [p2 H2]. rewrite H2.
      eapply discard_free_unitary in H0 as [p1 H1]. rewrite H1.
      apply Hu.
    + destruct (c x), (Gamma x); apply Hu.
    + destruct (type_check Gamma e1) as [T |]; try apply Hu.
      destruct (type_check Gamma e2) as [T0' |]; try apply Hu.
      destruct (type_check Gamma e3) as [T1' |]; try apply Hu.
      destruct (dup_inter Gamma c (free_vars e1) (remove x0 (free_vars e2)) i)
               as [[p new_i] new_c].
      eapply discard_free_unitary in H0 as [p' H]. rewrite H.
      eapply discard_free_unitary in H1 as [p0 H0]. rewrite H0.
      eapply discard_free_unitary in H2 as [p1 H1]. rewrite H1.
      simpl. apply Hu.
    + destruct (dup_ptrs Gamma c e1 e2 i) as [[p new_i] new_c].
      eapply discard_free_unitary in H0 as [p' H]. rewrite H.
      eapply discard_free_unitary in H1 as [p0 H0]. rewrite H0.
      eapply discard_free_unitary in H2 as [p1 H1]. rewrite H1.
      simpl. apply Hu.
    + destruct (type_check Gamma e1) as [[| | T1 T2] |]; try apply Hu.
      destruct (string_dec x1 x2); try apply Hu.
      destruct (type_check (x1 |-> T1, x2 |-> T2, Gamma) e2) as [T' |];
      try apply Hu.
      destruct (dup_inter Gamma
                          c
                          (free_vars e1)
                          (remove x1 (remove x2 (free_vars e2)))
                          i)
               as [[p new_i] new_c].
      destruct (appears_free_in_dec x1 e2); try contradiction.
      destruct (appears_free_in_dec x2 e2); try contradiction.
      clear H0 H1.
      eapply discard_free_unitary in H2 as [p1 H1]. rewrite H1.
      eapply discard_free_unitary in H3 as [p2 H2]. rewrite H2.
      simpl. apply Hu.
    + destruct (type_check Gamma e) as [T |]; try apply Hu.
      destruct (type_check_fn Gamma f) as [[T1 T'] |]; try apply Hu.
      destruct (ty_dec T1 T); try apply Hu. subst.
      destruct (dup_ptrs_fn Gamma c f e i) as [[p new_i] new_c].
      eapply discard_free_unitary in H1 as [pe H]. rewrite H.
      eapply discard_free_fn_unitary in H0 as [pf H']. rewrite H'.
      simpl. apply Hu.
  - intros f H dim Gamma c i o. destruct f; inversion_clear H; simpl.
    + destruct (appears_free_in_dec x e); try contradiction.
      apply discard_free_unitary. assumption.
    + exists (swap i o >> Some (uapp1 (gate2unitary g) o)). reflexivity.
Qed.
