(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import Maps.
Require Import QuantumOne.

(** An expression is discard-free if it uses all of the variables it creates. *)
Inductive discard_free : expr -> Prop :=
  | df_unit :
      discard_free <{ <> }>
  | df_inl T e :
      discard_free e ->
      discard_free <{ inl T e }>
  | df_inr T e :
      discard_free e ->
      discard_free <{ inr T e }>
  | df_pair e1 e2 :
      discard_free e1 ->
      discard_free e2 ->
      discard_free <{ <e1, e2> }>
  | df_var x :
      discard_free (expr_var x)
  | df_ctrl e x0 e0 x1 e1 :
      discard_free e ->
      discard_free e0 ->
      discard_free e1 ->
      discard_free <{ control e with | inl x0 => e0 | inr x1 => e1 }>
  | df_ctrl_bit e e0 e1 :
      discard_free e ->
      discard_free e0 ->
      discard_free e1 ->
      discard_free <{ control e with | 0 => e0 | 1 => e1 }>
  | df_let x1 x2 e' e :
      appears_free_in x1 e ->
      appears_free_in x2 e ->
      discard_free e' ->
      discard_free e ->
      discard_free <{ let <x1, x2> = e' in e }>
  | df_app f e :
      discard_free_fn f ->
      discard_free e ->
      discard_free <{ f e }>
with discard_free_fn : fn -> Prop :=
  | df_abs x T e :
      appears_free_in x e ->
      discard_free e ->
      discard_free_fn <{ \x : T, e }>
  | df_gate (g : gate) :
      discard_free_fn g.

Definition discard_free_dec :
  forall e, {discard_free e} + {~ discard_free e}.
Proof.
  intros e.
  induction e using expr_fn_rec
              with (P0 := fun f => {discard_free_fn f} + {~ discard_free_fn f}).
  - left. constructor.
  - destruct IHe.
    + left. constructor. assumption.
    + right. intros H. inversion H. contradiction.
  - destruct IHe.
    + left. constructor. assumption.
    + right. intros H. inversion H. contradiction.
  - destruct IHe1, IHe2; try (left; constructor; assumption);
    right; intros H; inversion H; contradiction.
  - left. constructor.
  - right. intros H. inversion H.
  - right. intros H. inversion H.
  - destruct IHe1, IHe2, IHe3; try (left; constructor; assumption);
    right; intros H; inversion H; contradiction.
  - destruct IHe1, IHe2, IHe3; try (left; constructor; assumption);
    right; intros H; inversion H; contradiction.
  - destruct (appears_free_in_dec x1 e2), (appears_free_in_dec x2 e2),
             IHe1, IHe2;
    try (left; constructor; assumption);
    right; intros H; inversion H; contradiction.
  - destruct IHe, IHe0; try (left; constructor; assumption);
    right; intros H; inversion H; contradiction.
  - destruct (appears_free_in_dec x e), IHe;
    try (left; constructor; assumption);
    right; intros H; inversion H; contradiction.
  - left. constructor.
Defined.

Definition context := partial_map ty.

(** There are two different typing relations, one for expressions and one for
    functions. *)
Reserved Notation "Gamma |- e : T"
    (at level 40, e custom q1, T custom q1_ty at level 0).
Reserved Notation "Gamma |> f : T"
    (at level 40, f custom q1, T custom q1_ty at level 0).
Inductive has_type : context -> expr -> ty -> Prop :=
  | has_type_unit Gamma :
      Gamma |- <> : Unit
  | has_type_inl Gamma e T0 T1 :
      Gamma |- e : T0 ->
      Gamma |- inl T1 e : (T0 + T1)
  | has_type_inr Gamma e T0 T1 :
      Gamma |- e : T1 ->
      Gamma |- inr T0 e : (T0 + T1)
  | has_type_pair Gamma e1 e2 T1 T2 :
      Gamma |- e1 : T1 ->
      Gamma |- e2 : T2 ->
      Gamma |- <e1, e2> : (T1 * T2)
  | has_type_var Gamma x T :
      Gamma x = Some T ->
      Gamma |- x : T
  | has_type_case Gamma e x0 e0 x1 e1 T0 T1 T :
      Gamma |- e : (T0 + T1) ->
      (x0 |-> T0; Gamma) |- e0 : T ->
      (x1 |-> T1; Gamma) |- e1 : T ->
      Gamma |- case e of | inl x0 => e0 | inr x1 => e1 : T
  | has_type_if Gamma e e0 e1 T :
      Gamma |- e : Bit ->
      Gamma |- e0 : T ->
      Gamma |- e1 : T ->
      Gamma |- case e of | 0 => e0 | 1 => e1 : T
  | has_type_ctrl Gamma e x0 e0 x1 e1 T0 T1 T :
      (forall x,
       x <> x0 /\ appears_free_in x e0 <-> x <> x1 /\ appears_free_in x e1) ->
      discard_free e0 ->
      discard_free e1 ->
      Gamma |- e : (T0 + T1) ->
      (x0 |-> T0; Gamma) |- e0 : T ->
      (x1 |-> T1; Gamma) |- e1 : T ->
      Gamma |- control e with | inl x0 => e0 | inr x1 => e1 : ((T0 + T1) * T)
  | has_type_ctrl_bit Gamma e e0 e1 T :
      (forall x, appears_free_in x e0 <-> appears_free_in x e1) ->
      discard_free e0 ->
      discard_free e1 ->
      Gamma |- e : Bit ->
      Gamma |- e0 : T ->
      Gamma |- e1 : T ->
      Gamma |- control e with | 0 => e0 | 1 => e1 : (Bit * T)
  | has_type_let Gamma x1 x2 e' e T1 T2 T :
      x1 <> x2 ->
      Gamma |- e' : (T1 * T2) ->
      (x1 |-> T1; x2 |-> T2; Gamma) |- e : T ->
      Gamma |- let <x1, x2> = e' in e : T
  | has_type_app Gamma f e T T' :
      Gamma |> f : (T -> T') ->
      Gamma |- e : T ->
      Gamma |- f e : T'
where "Gamma |- e : T" := (has_type Gamma e T)
with has_type_fn : context -> fn -> ty_fn -> Prop :=
  | has_type_abs Gamma x e T T' :
      (x |-> T; Gamma) |- e : T' ->
      Gamma |> \x : T, e : (T -> T')
  | has_type_gate Gamma (g : gate) :
      Gamma |> g : (Bit -> Bit)
where "Gamma |> f : F" := (has_type_fn Gamma f F).

Notation "|- e : T" :=
    (empty |- e : T)
    (at level 40, e custom q1, T custom q1_ty at level 0).
Notation "|> f : F" :=
    (empty |> f : F)
    (at level 40, f custom q1, F custom q1_ty at level 0).

Scheme has_type_expr_fn_ind := Induction for has_type Sort Prop
  with has_type_fn_expr_ind := Induction for has_type_fn Sort Prop.

Lemma has_type_0 :
  forall Gamma,
  Gamma |- 0 : Bit.
Proof. eauto using has_type. Qed.

Lemma has_type_1 :
  forall Gamma,
  Gamma |- 1 : Bit.
Proof. eauto using has_type. Qed.

Lemma weakening :
  forall Gamma Gamma' e T,
  inclusion Gamma Gamma' ->
  Gamma |- e : T ->
  Gamma' |- e : T.
Proof.
  intros Gamma Gamma' e T Hi He. generalize dependent Gamma'.
  induction He using has_type_expr_fn_ind
               with (P0 :=
                     fun Gamma f T (H : Gamma |> f : T) =>
                     forall Gamma', inclusion Gamma Gamma' -> Gamma' |> f : T);
  eauto using has_type;
  intros Gamma' Hi; econstructor; auto using inclusion_update.
Qed.

Lemma weakening_fn :
  forall Gamma Gamma' f T,
  inclusion Gamma Gamma' ->
  Gamma |> f : T ->
  Gamma' |> f : T.
Proof.
  intros Gamma Gamma' f T Hi He. generalize dependent Gamma'.
  induction He using has_type_fn_expr_ind
               with (P :=
                     fun Gamma e T (H : Gamma |- e : T) =>
                     forall Gamma', inclusion Gamma Gamma' -> Gamma' |- e : T);
  eauto using has_type;
  intros Gamma' Hi; econstructor; auto using inclusion_update.
Qed.

Lemma weakening_empty :
  forall Gamma e T,
  |- e : T ->
  Gamma |- e : T.
Proof.
  intros Gamma e T H. apply weakening with (Gamma := empty).
  - discriminate.
  - assumption.
Qed.

Lemma weakening_fn_empty :
  forall Gamma f T,
  |> f : T ->
  Gamma |> f : T.
Proof.
  intros Gamma f T H. apply weakening_fn with (Gamma := empty).
  - discriminate.
  - assumption.
Qed.

Create HintDb type_db.

#[export] Hint Constructors has_type has_type_fn eq : type_db.
#[export] Hint Resolve has_type_0 has_type_1 weakening_empty
                       weakening_fn_empty :
  type_db.

(* https://softwarefoundations.cis.upenn.edu/plf-current/Typechecking.html *)
Notation "x <- e1 ;; e2" := (match e1 with
                              | Some x => e2
                              | None => None
                              end)
                            (right associativity, at level 60).

(* Compute the (deterministic) type. *)
Fixpoint type_check (Gamma : context) e :=
  match e with
  | <{ <> }> => Some <{{ Unit }}>
  | <{ inl T2 e' }> => T1 <- type_check Gamma e' ;; Some <{{ T1 + T2 }}>
  | <{ inr T1 e' }> => T2 <- type_check Gamma e' ;; Some <{{ T1 + T2 }}>
  | <{ <e1, e2> }> =>
      T1 <- type_check Gamma e1 ;;
      T2 <- type_check Gamma e2 ;;
      Some <{{ T1 * T2 }}>
  | expr_var x => Gamma x
  | <{ case e' of | inl x0 => e0 | inr x1 => e1 }> =>
      T <- type_check Gamma e' ;;
      match T with
      | <{{ T0 + T1 }}> =>
          T0' <- type_check (x0 |-> T0; Gamma) e0 ;;
          T1' <- type_check (x1 |-> T1; Gamma) e1 ;;
          if ty_dec T0' T1' then Some T0' else None
      | _ => None
      end
  | <{ case e' of | 0 => e0 | 1 => e1 }> =>
      T <- type_check Gamma e' ;;
      match T with
      | <{{ Bit }}> =>
          T0' <- type_check Gamma e0 ;;
          T1' <- type_check Gamma e1 ;;
          if ty_dec T0' T1' then Some T0' else None
      | _ => None
      end
  | <{ control e' with | inl x0 => e0 | inr x1 => e1 }> =>
      T <- type_check Gamma e' ;;
      match T with
      | <{{ T0 + T1 }}> =>
          T0' <- type_check (x0 |-> T0; Gamma) e0 ;;
          T1' <- type_check (x1 |-> T1; Gamma) e1 ;;
          if ty_dec T0' T1'
          then if S.equal (S.remove x0 (free_vars e0))
                          (S.remove x1 (free_vars e1))
               then if discard_free_dec e0
                    then if discard_free_dec e1
                         then Some <{{ T * T0' }}>
                         else None
                    else None
               else None
          else None
      | _ => None
      end
  | <{ control e' with | 0 => e0 | 1 => e1 }> =>
      T <- type_check Gamma e' ;;
      match T with
      | <{{ Bit }}> =>
          T0' <- type_check Gamma e0 ;;
          T1' <- type_check Gamma e1 ;;
          if ty_dec T0' T1'
          then if S.equal (free_vars e0) (free_vars e1)
               then if discard_free_dec e0
                    then if discard_free_dec e1
                         then Some <{{ Bit * T0' }}>
                         else None
                    else None
               else None
          else None
      | _ => None
      end
  | <{ let <x1, x2> = e' in e }> =>
      T <- type_check Gamma e' ;;
      match T with
      | <{{ T1 * T2 }}> =>
          if string_dec x1 x2
          then None
          else type_check (x1 |-> T1; x2 |-> T2; Gamma) e
      | _ => None
      end
  | <{ f e }> =>
      F <- type_check_fn Gamma f ;;
      T <- type_check Gamma e ;;
      match F with
      | <{{ T1 -> T2 }}> => if ty_dec T1 T then Some T2 else None
      end
  end
with type_check_fn Gamma f :=
  match f with
  | <{ \x : T, e }> =>
      T' <- type_check (x |-> T; Gamma) e ;;
      Some <{{ T -> T' }}>
  | fn_gate g => Some <{{ Bit -> Bit }}>
  end.

Lemma type_check_correct :
  forall Gamma e T,
  Gamma |- e : T <->
  type_check Gamma e = Some T.
Proof.
  intros Gamma e T. split.
  - intros H.
    induction H using has_type_expr_fn_ind
                with (P0 := fun Gamma f F (H : Gamma |> f : F) =>
                            type_check_fn Gamma f = Some F);
    simpl;
    try rewrite IHhas_type; try rewrite IHhas_type0;
    try rewrite IHhas_type1, IHhas_type2; try rewrite IHhas_type3;
    try (destruct (string_dec x1 x2); congruence); auto using ty_dec_refl;
    rewrite ty_dec_refl;
    destruct (discard_free_dec e0); try contradiction;
    destruct (discard_free_dec e1); try contradiction;
    replace (S.equal _ _) with true; try reflexivity;
    symmetry; apply S.equal_spec; intros x; specialize (i x);
    repeat rewrite S.remove_spec; repeat rewrite free_vars_correct; tauto.
  - generalize dependent T. generalize dependent Gamma.
    induction e using expr_fn_ind
                with (P0 := fun f =>
                            forall Gamma F,
                            type_check_fn Gamma f = Some F -> Gamma |> f : F);
    simpl; intros Gamma T' H.
    + injection H as. subst. constructor.
    + destruct (type_check Gamma e) as [T0 |] eqn:E; try discriminate.
      injection H as. subst. eauto using has_type.
    + destruct (type_check Gamma e) as [T1 |] eqn:E; try discriminate.
      injection H as. subst. eauto using has_type.
    + destruct (type_check Gamma e1) as [T1 |] eqn:E1; try discriminate.
      destruct (type_check Gamma e2) as [T2 |] eqn:E2; try discriminate.
      injection H as. subst. eauto using has_type.
    + eauto using has_type.
    + destruct (type_check Gamma e1) as [T |] eqn:E; try discriminate.
      destruct T as [| T0 T1 |]; try discriminate.
      destruct (type_check (x0 |-> T0; Gamma) e2) as [T0' |] eqn:E0;
      try discriminate.
      destruct (type_check (x1 |-> T1; Gamma) e3) as [T1' |] eqn:E1;
      try discriminate.
      destruct (ty_dec T0' T1'); try discriminate. injection H as. subst.
      eauto using has_type.
    + destruct (type_check Gamma e1) as [T |] eqn:E; try discriminate.
      destruct T as [| T0 T1 |]; try discriminate.
      destruct T0, T1; try discriminate.
      destruct (type_check Gamma e2) as [T0' |] eqn:E0;
      try discriminate.
      destruct (type_check Gamma e3) as [T1' |] eqn:E1;
      try discriminate.
      destruct (ty_dec T0' T1'); try discriminate. injection H as. subst.
      eauto using has_type.
    + destruct (type_check Gamma e1) as [T |] eqn:E; try discriminate.
      destruct T as [| T0 T1 |]; try discriminate.
      destruct (type_check (x0 |-> T0; Gamma) e2) as [T0' |] eqn:E0;
      try discriminate.
      destruct (type_check (x1 |-> T1; Gamma) e3) as [T1' |] eqn:E1;
      try discriminate.
      destruct (ty_dec T0' T1'); try discriminate.
      destruct (S.equal _ _) eqn:Ee; try discriminate.
      destruct (discard_free_dec e2), (discard_free_dec e3); try discriminate.
      injection H as. subst.
      constructor; eauto using has_type.
      apply S.equal_spec in Ee.
      intros x. specialize (Ee x).
      repeat rewrite S.remove_spec, free_vars_correct in Ee. tauto.
    + destruct (type_check Gamma e1) as [T |] eqn:E; try discriminate.
      destruct T as [| T0 T1 |]; try discriminate.
      destruct T0, T1; try discriminate.
      destruct (type_check Gamma e2) as [T0' |] eqn:E0;
      try discriminate.
      destruct (type_check Gamma e3) as [T1' |] eqn:E1;
      try discriminate.
      destruct (ty_dec T0' T1'); try discriminate.
      destruct (S.equal _ _) eqn:Ee; try discriminate.
      destruct (discard_free_dec e2), (discard_free_dec e3); try discriminate.
      injection H as. subst.
      constructor; eauto using has_type.
      apply S.equal_spec in Ee.
      intros x. specialize (Ee x).
      repeat rewrite free_vars_correct in Ee. tauto.
    + destruct (type_check Gamma e1) as [T |] eqn:E; try discriminate.
      destruct T as [| | T1 T2]; try discriminate.
      destruct (string_dec x1 x2); try discriminate.
      destruct (type_check (x1 |-> T1; x2 |-> T2; Gamma) e2) as [T |] eqn:E';
      try discriminate.
      injection H as. subst. eauto using has_type.
    + destruct (type_check_fn Gamma f) as [F |] eqn:Ef; try discriminate.
      destruct (type_check Gamma e) as [T |] eqn:Ee; try discriminate.
      destruct F. destruct (ty_dec T0 T); try discriminate.
      injection H as. subst. eauto using has_type.
    + rename T' into F.
      destruct (type_check (x |-> T; Gamma) e) as [T' |] eqn:E;
      try discriminate.
      injection H as. subst. auto using has_type_fn.
    + injection H as. subst. constructor.
Qed.

Lemma type_check_fn_correct : 
  forall Gamma f F,
  Gamma |> f : F <->
  type_check_fn Gamma f = Some F.
Proof.
  intros Gamma f F. split; destruct f; simpl; intros H.
  - inversion_clear H. apply type_check_correct in H0. rewrite H0. reflexivity.
  - inversion_clear H. reflexivity.
  - destruct (type_check (x |-> T; Gamma)) as [T' |] eqn:E; try discriminate.
    injection H as. subst. constructor. apply type_check_correct. assumption.
  - injection H as. subst. constructor.
Qed.

Ltac type_check :=
  intros;
  match goal with
  | [ |- has_type ?Gamma ?e ?T ] => apply type_check_correct; reflexivity
  | [ |- has_type_fn ?Gamma ?e ?T ] => apply type_check_fn_correct; reflexivity
  end.
