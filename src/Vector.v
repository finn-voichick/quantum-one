(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import RelationClasses.
Require Import Complex.

Section Vector.

Variable X : Set.
Hypothesis eq_dec : forall x x' : X, {x = x'} + {x <> x'}.

Inductive vector :=
  | vector_scale (alpha : C) (x : X)
  | vector_sum (v0 v1 : vector).

(** "ket" just defines a basis vector. *)
Definition ket x := vector_scale 1 x.

Fixpoint scale alpha v :=
  match v with
  | vector_scale beta x => vector_scale (alpha * beta) x
  | vector_sum v0 v1 => vector_sum (scale alpha v0) (scale alpha v1)
  end.

Fixpoint amplitude v x :=
  match v with
  | vector_scale alpha x' => if eq_dec x x' then alpha else 0
  | vector_sum v0 v1 => amplitude v0 x + amplitude v1 x
  end.

(** Vector equivalence is defined in terms of amplitudes. *)
Definition vector_equiv v v' := amplitude v = amplitude v'.

Instance vector_equiv_refl :
  Reflexive vector_equiv. 
Proof.
  intros v. reflexivity.
Qed.

Instance vector_equiv_sym :
  Symmetric vector_equiv.
Proof.
  intros v v' H. unfold vector_equiv in *. symmetry. assumption.
Qed.

Instance vector_equiv_trans :
  Transitive vector_equiv.
Proof.
  intros v1 v2 v3 H1 H3. unfold vector_equiv in *.
  transitivity (amplitude v2); assumption.
Qed.

(* This should only be used for injective functions. *)
Fixpoint vmap f v :=
  match v with
  | vector_scale alpha x => vector_scale alpha (f x)
  | vector_sum v0 v1 => vector_sum (vmap f v0) (vmap f v1)
  end.

(* Like a monadic bind. *)
Fixpoint vbind f v :=
  match v with
  | vector_scale alpha x => scale alpha (f x)
  | vector_sum v0 v1 => vector_sum (vbind f v0) (vbind f v1)
  end.

Fixpoint vsum f v :=
  match v with
  | vector_scale alpha x => f alpha x
  | vector_sum v0 v1 => vsum f v0 + vsum f v1
  end.

Definition density := X -> X -> C.

End Vector.

Arguments vector_scale {X}.
Arguments vector_sum {X}.
Arguments ket {X}.
Arguments scale {X}.
Arguments vmap {X}.
Arguments vsum {X}.
