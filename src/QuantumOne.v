(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import Reals Psatz MSets OrderedTypeEx.
Require Import Maps.
(* https://stackoverflow.com/a/44798927 *)
Module String_as_OT := Update_OT String_as_OT.
Module S := Make String_as_OT.
Import S.

(** Only single-qubit gates are defined because entangling operations can be
    produced using other language constructs. *)
Inductive gate :=
  | U_R (theta phi lambda : R).

Definition R_dec :
  forall r r' : R, {r = r'} + {r <> r'}.
Proof.
  intros r r'. destruct (Req_appart_dec r r'); auto.
  right; lra.
Defined.

Definition gate_dec :
  forall g g' : gate, {g = g'} + {g <> g'}.
Proof.
  decide equality; apply R_dec.
Qed.

(** There are no higher-order functions, so function types are separate. *)
Inductive ty : Set :=
  | ty_unit
  | ty_sum (T0 T1 : ty)
  | ty_prod (T1 T2 : ty).

Definition ty_dec :
  forall T T' : ty, {T = T'} + {T <> T'}.
Proof. decide equality. Defined.

Lemma ty_dec_refl :
  forall X (a b : X) T, (if ty_dec T T then a else b) = a.
Proof.
  intros X a b T. induction T as [| T0 IH0 T1 IH1 | T1 IH1 T2 IH2]; simpl.
  - reflexivity.
  - destruct (ty_dec T0 T0), (ty_dec T1 T1); auto.
  - destruct (ty_dec T1 T1), (ty_dec T2 T2); auto.
Qed.

Inductive ty_fn : Set :=
  | ty_arr (T T' : ty).

Declare Custom Entry q1_ty.

Notation "<{{ T }}>" := T (T custom q1_ty at level 99).
Notation "( x )" := x (in custom q1_ty, x at level 99).
Notation "x" := x (in custom q1_ty at level 0, x constr at level 0).
Notation "'Unit'" := ty_unit (in custom q1_ty).
Notation "T0 + T1" :=
  (ty_sum T0 T1)
  (in custom q1_ty at level 3,
   left associativity).
Notation "T1 * T2" :=
  (ty_prod T1 T2)
  (in custom q1_ty at level 2,
   T1 custom q1_ty,
   T2 custom q1_ty at level 0).
Notation "'Bit'" := <{{ Unit + Unit }}> (in custom q1_ty). 
Notation "T -> T'" := (ty_arr T T') (in custom q1_ty at level 50).
Notation "{ T }" := T (in custom q1_ty, T constr).

Reserved Notation "'Array' < T > [ n ]" (in custom q1_ty).
Fixpoint array T n :=
  match n with
  | 0 => <{{ Unit }}>
  | S n' => <{{ T * Array<T>[n'] }}>
  end
where "'Array' < T > [ n ]" := (array T n) (in custom q1_ty).

(** Registers are just arrays of bits. *)
Notation "'Reg' [ n ]" := <{{ Array<Bit>[n] }}>.

(** Functions are first-order only. Expressions and functions are defined
    mutually inductively. *)
Inductive expr : Set :=
  | expr_unit
  | expr_inl (T1 : ty) (e : expr) 
  | expr_inr (T0 : ty) (e : expr)
  | expr_pair (e1 e2 : expr)
  | expr_var (x : string)
  | expr_case (e : expr) (x0 : string) (e0 : expr) (x1 : string) (e1 : expr)
  | expr_if (e e0 e1 : expr)
  | expr_ctrl (e : expr) (x0 : string) (e0 : expr) (x1 : string) (e1 : expr)
  | expr_ctrl_bit (e e0 e1 : expr)
  | expr_let (x1 x2 : string) (e' e : expr)
  | expr_app (f : fn) (e : expr)
with fn : Set :=
  | fn_abs (x : string) (T : ty) (e : expr)
  | fn_gate (g : gate).

Declare Custom Entry q1.

Notation "<{ e }>" := e (e custom q1 at level 99).
Notation "x" := x (in custom q1 at level 0, x constr at level 0).
Notation "( x )" := x (in custom q1, x at level 99).
Notation "<>" := expr_unit (in custom q1).
Notation "'inl' T e" :=
  (expr_inl T e) (in custom q1 at level 0, T custom q1_ty).
Notation "'inr' T e" :=
  (expr_inr T e) (in custom q1 at level 0, T custom q1_ty).
Notation "< e1 , e2 >" := (expr_pair e1 e2) (in custom q1).
Coercion expr_var : string >-> expr.
Notation "'case' e 'of' | 'inl' x0 => e0 | 'inr' x1 => e1" :=
  (expr_case e x0 e0 x1 e1)
  (in custom q1 at level 89,
   e custom q1 at level 99,
   x0 custom q1 at level 99,
   e0 custom q1 at level 99,
   x1 custom q1 at level 99,
   e1 custom q1 at level 99,
   left associativity).
Notation "'case' e 'of' | 0 => e0 | 1 => e1" :=
  (expr_if e e0 e1)
  (in custom q1 at level 89,
   e custom q1 at level 99,
   e0 custom q1 at level 99,
   e1 custom q1 at level 99,
   left associativity).
Notation "'control' e 'with' | 'inl' x0 => e0 | 'inr' x1 => e1" :=
  (expr_ctrl e x0 e0 x1 e1)
  (in custom q1 at level 89,
   e custom q1 at level 99,
   x0 custom q1 at level 99,
   e0 custom q1 at level 99,
   x1 custom q1 at level 99,
   e1 custom q1 at level 99,
   left associativity).
Notation "'control' e 'with' | 0 => e0 | 1 => e1" :=
  (expr_ctrl_bit e e0 e1)
  (in custom q1 at level 89,
   e custom q1 at level 99,
   e0 custom q1 at level 99,
   e1 custom q1 at level 99,
   left associativity).
Notation "'let' < x1 , x2 > = e' 'in' e" :=
  (expr_let x1 x2 e' e) (in custom q1 at level 0).
Notation "f e" :=
  (expr_app f e) (in custom q1 at level 1, left associativity).
Notation "\ x : T , e" :=
  (fn_abs x T e)
  (in custom q1 at level 90,
   x at level 99,
   T custom q1_ty,
   e custom q1 at level 99,
   left associativity).
Coercion fn_gate : gate >-> fn.
Notation "{ x }" := x (in custom q1, x constr).

(** 0 and 1 are defined in terms of disjoint unions. *)
Notation "0" := <{ inl Unit <> }> (in custom q1).
Notation "1" := <{ inr Unit <> }> (in custom q1).

(** The mutually inductive definition means that inductive proofs are a little
    bit trickier. *)
Scheme expr_fn_rec := Induction for expr Sort Set
  with fn_expr_rec := Induction for fn Sort Set.

Scheme expr_fn_ind := Induction for expr Sort Prop
  with fn_expr_ind := Induction for fn Sort Prop.

Module VarNames.

  Definition x : string := "x".
  Definition x' : string := "x'".
  Definition x0 : string := "x0".
  Definition x1 : string := "x1".
  Definition x2 : string := "x2".
  Definition y : string := "y".

End VarNames.

Definition expr_dec :
  forall e e' : expr, {e = e'} + {e <> e'}.
Proof.
  intros e.
  induction e using expr_fn_rec
              with (P0 := fun f => forall f' : fn, {f = f'} + {f <> f'});
  try intros []; try (right; discriminate); auto.
  - destruct (IHe e0), (ty_dec T1 T0); subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (IHe e0), (ty_dec T0 T1); subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (IHe1 e0), (IHe2 e3); subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (string_dec x x0); subst; auto.
    right. intros H. injection H as. contradiction.
  - destruct (IHe1 e),
             (string_dec x0 x2), (IHe2 e0),
             (string_dec x1 x3), (IHe3 e4);
    subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (IHe1 e), (IHe2 e0), (IHe3 e4); subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (IHe1 e),
             (string_dec x0 x2), (IHe2 e0),
             (string_dec x1 x3), (IHe3 e4);
    subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (IHe1 e), (IHe2 e0), (IHe3 e4); subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (string_dec x1 x0), (string_dec x2 x3), (IHe1 e'), (IHe2 e);
    subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (IHe f0), (IHe0 e0); subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (string_dec x x0), (ty_dec T T0), (IHe e0); subst; auto;
    right; intros H; injection H as; contradiction.
  - destruct (gate_dec g g0); subst; auto.
    right; intros H; injection H as; contradiction.
Defined.

Inductive appears_free_in (x : string) : expr -> Prop :=
  | afi_var : appears_free_in x <{ x }>
  | afi_inl T e :
      appears_free_in x e ->
      appears_free_in x <{ inl T e }>
  | afi_inr T e :
      appears_free_in x e ->
      appears_free_in x <{ inr T e }>
  | afi_pair1 e1 e2 :
      appears_free_in x e1 ->
      appears_free_in x <{ <e1, e2> }>
  | afi_pair2 e1 e2 :
      appears_free_in x e2 ->
      appears_free_in x <{ <e1, e2> }>
  | afi_case1 e x0 e0 x1 e1 :
      appears_free_in x e ->
      appears_free_in x <{ case e of | inl x0 => e0 | inr x1 => e1 }>
  | afi_case2 e x0 e0 x1 e1 :
      x <> x0 ->
      appears_free_in x e0 ->
      appears_free_in x <{ case e of | inl x0 => e0 | inr x1 => e1 }>
  | afi_case3 e x0 e0 x1 e1 :
      x <> x1 ->
      appears_free_in x e1 ->
      appears_free_in x <{ case e of | inl x0 => e0 | inr x1 => e1 }>
  | afi_if1 e e0 e1 :
      appears_free_in x e ->
      appears_free_in x <{ case e of | 0 => e0 | 1 => e1 }>
  | afi_if2 e e0 e1 :
      appears_free_in x e0 ->
      appears_free_in x <{ case e of | 0 => e0 | 1 => e1 }>
  | afi_if3 e e0 e1 :
      appears_free_in x e1 ->
      appears_free_in x <{ case e of | 0 => e0 | 1 => e1 }>
  | afi_ctrl1 e x0 e0 x1 e1 :
      appears_free_in x e ->
      appears_free_in x <{ control e with | inl x0 => e0 | inr x1 => e1 }>
  | afi_ctrl2 e x0 e0 x1 e1 :
      x <> x0 ->
      appears_free_in x e0 ->
      appears_free_in x <{ control e with | inl x0 => e0 | inr x1 => e1 }>
  | afi_ctrl3 e x0 e0 x1 e1 :
      x <> x1 ->
      appears_free_in x e1 ->
      appears_free_in x <{ control e with | inl x0 => e0 | inr x1 => e1 }>
  | afi_ctrl_bit1 e e0 e1 :
      appears_free_in x e ->
      appears_free_in x <{ control e with | 0 => e0 | 1 => e1 }>
  | afi_ctrl_bit2 e e0 e1 :
      appears_free_in x e0 ->
      appears_free_in x <{ control e with | 0 => e0 | 1 => e1 }>
  | afi_ctrl_bit3 e e0 e1 :
      appears_free_in x e1 ->
      appears_free_in x <{ control e with | 0 => e0 | 1 => e1 }>
  | afi_let1 x1 x2 e' e :
      appears_free_in x e' ->
      appears_free_in x <{ let <x1, x2> = e' in e }>
  | afi_let2 x1 x2 e' e :
      x <> x1 ->
      x <> x2 ->
      appears_free_in x e ->
      appears_free_in x <{ let <x1, x2> = e' in e }>
  | afi_app1 f e :
      appears_free_in_fn x f ->
      appears_free_in x <{ f e }>
  | afi_app2 f e :
      appears_free_in x e ->
      appears_free_in x <{ f e }>
with appears_free_in_fn (x : string) : fn -> Prop :=
  | afi_abs x' T e :
      x <> x'  ->
      appears_free_in x e ->
      appears_free_in_fn x <{ \x' : T, e }>.

Fixpoint free_vars e :=
  match e with
  | <{ <> }> => empty
  | <{ inl T e' }> => free_vars e'
  | <{ inr T e' }> => free_vars e'
  | <{ <e1, e2> }> => union (free_vars e1) (free_vars e2)
  | expr_var x => singleton x
  | <{ case e' of | inl x0 => e0 | inr x1 => e1 }> =>
      union (free_vars e')
            (union (remove x0 (free_vars e0)) (remove x1 (free_vars e1)))
  | <{ case e' of | 0 => e0 | 1 => e1 }> =>
      union (free_vars e') (union (free_vars e0) (free_vars e1))
  | <{ control e' with | inl x0 => e0 | inr x1 => e1 }> =>
      union (free_vars e')
            (union (remove x0 (free_vars e0)) (remove x1 (free_vars e1)))
  | <{ control e' with | 0 => e0 | 1 => e1 }> =>
      union (free_vars e') (union (free_vars e0) (free_vars e1))
  | <{ let <x1, x2> = e1 in e2 }> =>
      union (free_vars e1) (remove x1 (remove x2 (free_vars e2)))
  | <{ f e }> => union (free_vars_fn f) (free_vars e)
  end
with free_vars_fn f :=
  match f with
  | <{ \x : T, e1 }> => remove x (free_vars e1)
  | fn_gate g => empty
  end.

Scheme afi_ind := Induction for appears_free_in Sort Prop
  with afi_fn_ind := Induction for appears_free_in_fn Sort Prop.

Lemma free_vars_correct :
  forall x e, In x (free_vars e) <-> appears_free_in x e.
Proof.
  intros x e. split.
  - induction e using expr_fn_ind
    with (P0 := fun f => In x (free_vars_fn f) -> appears_free_in_fn x f);
    simpl; intros H; try apply empty_spec in H; try rewrite singleton_spec in H;
    repeat rewrite union_spec in H; repeat rewrite remove_spec in H;
    repeat (auto using appears_free_in, appears_free_in_fn; destruct H).
  - intros H.
    induction H using afi_ind
                with (P0 := fun f (H : appears_free_in_fn x f) =>
                            In x (free_vars_fn f));
    simpl; repeat rewrite union_spec; repeat rewrite remove_spec; auto.
    apply singleton_spec. reflexivity.
Qed.

Definition appears_free_in_dec :
  forall x e, {appears_free_in x e} + {~ appears_free_in x e}.
Proof.
  intros x e. destruct (mem x (free_vars e)) eqn:E.
  - left. apply free_vars_correct. apply mem_spec. assumption.
  - right. intros H. apply free_vars_correct in H. apply mem_spec in H.
    congruence.
Defined.

