(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import Nat FinFun.
Require Import Complex.
Require Import DensitySem.
Require Import QuantumOne src.Vector Semantics src.Types.

Open Scope nat_scope.

(** How many values exist of a given type? *)
Fixpoint cardinal T : nat :=
  match T with
  | <{{ Unit }}> => 1
  | <{{ T0 + T1 }}> => cardinal T0 + cardinal T1
  | <{{ T1 * T2 }}> => cardinal T1 * cardinal T2
  end.

Example ex_cardinal :
  cardinal <{{ Unit + Bit * Bit }}> = 5%nat.
Proof. reflexivity. Qed.

(* A list of all values of a given type. *)
Fixpoint enumerate T : list expr :=
  match T with
  | <{{ Unit }}> => <{ <> }> :: []
  | <{{ T0 + T1 }}> =>
      map (expr_inl T1) (enumerate T0) ++ map (expr_inr T0) (enumerate T1)
  | <{{ T1 * T2 }}> => 
      let pair2pair :=
        fun (p : expr * expr) => let (e1, e2) := p in <{ <e1, e2> }>
      in
      map pair2pair (list_prod (enumerate T1) (enumerate T2))
  end.

Example ex_enumerate :
  enumerate <{{ Unit + Bit * Bit }}> =
  <{ inl (Bit * Bit) <> }> ::
  <{ inr Unit <0, 0> }> :: <{ inr Unit <0, 1> }> ::
  <{ inr Unit <1, 0> }> :: <{ inr Unit <1, 1> }> :: [].
Proof. reflexivity. Qed.

Lemma enumerate_cardinal :
  forall T, length (enumerate T) = cardinal T.
Proof.
  intros T. induction T.
  - reflexivity.
  - simpl. rewrite app_length, map_length, map_length, IHT1, IHT2. reflexivity.
  - simpl. rewrite map_length, prod_length, IHT1, IHT2. reflexivity.
Qed.

(* Number of qubits used to represent a given type *)
Fixpoint size T : nat :=
  match T with 
  | <{{ Unit }}> => 0
  | <{{ T0 + T1 }}> => 1 + max (size T0) (size T1)
  | <{{ T1 * T2 }}> => size T1 + size T2
  end.

Fixpoint dim T : nat :=
  match T with
  | <{{ Unit }}> => 1
  | <{{ T0 + T1 }}> => 2 * max (dim T0) (dim T1)
  | <{{ T1 * T2 }}> => dim T1 * dim T2
  end.

Lemma dim_nonzero :
  forall T, dim T <> 0.
Proof.
  intros T. induction T; simpl; lia.
Qed.

Lemma max_pow_dist :
  forall b n n', b <> 0 -> max (b ^ n) (b ^ n') = b ^ (max n n').
Proof.
  intros b n. induction n; simpl; intros n' H.
  - destruct (b ^ n') eqn:E; try reflexivity.
    apply Nat.pow_nonzero with (b := n') in H. contradiction.
  - destruct n'; simpl.
    + rewrite Nat.max_comm. simpl. destruct (b * b ^ n) eqn:E; try reflexivity.
      apply Nat.mul_eq_0 in E as [H0 | H0]; try contradiction.
      apply Nat.pow_nonzero with (b := n) in H. contradiction.
    + rewrite Nat.mul_max_distr_l, IHn; auto.
Qed.

Lemma dim_size :
  forall T, dim T = 2 ^ size T.
Proof.
  intros T. induction T as [| T0 IH0 T1 IH1 | T1 IH1 T2 IH2]; simpl.
  - reflexivity.
  - rewrite IH0, IH1. repeat rewrite <- plus_n_O.
    rewrite max_pow_dist; auto.
  - rewrite IH1, IH2. symmetry. apply Nat.pow_add_r.
Qed.

(* Whether an expression is a value with a given type. *)
Inductive value_in_type : expr -> ty -> Prop :=
  | vit_unit :
      value_in_type <{ <> }> <{{ Unit }}>
  | vit_inl e T0 T1 :
      value_in_type e T0 ->
      value_in_type <{ inl T1 e }> <{{ T0 + T1 }}>
  | vit_inr e T0 T1 :
      value_in_type e T1 ->
      value_in_type <{ inr T0 e }> <{{ T0 + T1 }}>
  | vit_pair e1 e2 T1 T2 :
      value_in_type e1 T1 ->
      value_in_type e2 T2 ->
      value_in_type <{ <e1, e2> }> <{{ T1 * T2 }}>.

Definition value_in_type_dec :
  forall e T, {value_in_type e T} + {~ value_in_type e T}.
Proof.
  intros e. induction e; intros T; try now (right; intros H; inversion H).
  - destruct T; auto using value_in_type; right; intros H; inversion H.
  - destruct T; try destruct (IHe T2), (ty_dec T1 T3);
    subst; auto using value_in_type;
    right; intros H; inversion H; contradiction.
  - destruct T; try destruct (IHe T2), (ty_dec T0 T1);
    subst; auto using value_in_type;
    right; intros H; inversion H; contradiction.
  - destruct T; try destruct (IHe1 T1), (IHe2 T2); auto using value_in_type;
    right; intros H; inversion H; contradiction.
Defined.

Lemma value_in_type_spec :
  forall e T,
  value_in_type e T <-> value e /\ |- e : T.
Proof.
  intros e T. split; intros H.
  - induction H as [| e T0 T1 H [] | e T0 T1 H [] | e1 e2 T1 T2 H1 [] H2 []];
    split; eauto using value, has_type.
  - generalize dependent T.
    induction e; intros T [Hv Ht]; try now (inversion Hv).
    + inversion Ht. constructor.
    + inversion Ht. constructor. apply IHe. split; try assumption.
      inversion Hv. assumption.
    + inversion Ht. constructor. apply IHe. split; try assumption.
      inversion Hv. assumption.
    + inversion Ht. constructor.
      * apply IHe1. split; try assumption.
        inversion Hv. assumption.
      * apply IHe2. split; try assumption.
        inversion Hv. assumption.
Qed.

Lemma value_in_type_enumerate :
  forall e T,
  value_in_type e T <-> In e (enumerate T).
Proof.
  intros e T. split; intros H.
  - induction H; simpl.
    + auto.
    + apply in_app_iff. left. apply in_map. assumption.
    + apply in_app_iff. right. apply in_map. assumption.
    + remember (fun _ => _) as f.
      replace <{ <e1, e2> }> with (f (e1, e2)) by (subst; reflexivity).
      apply in_map, in_prod; assumption.
  - generalize dependent e. induction T; simpl.
    + intros e []; try contradiction.
      subst. constructor.
    + intros e H. apply in_app_iff in H as [H | H];
      apply in_map_iff in H as [e' [He H]]; subst; auto using value_in_type.
    + intros e H. apply in_map_iff in H as [e' [He H]]. destruct e' as [e1 e2].
      subst. apply in_prod_iff in H as [H1 H2]. constructor; auto.
Qed.

(* The natural number corresponding to a value's bit representation. *)
Fixpoint value_index T v :=
  match T with
  | <{{ Unit }}> => match v with
                    | <{ <> }> => Some 0
                    | _ => None
                    end
  | <{{ T0 + T1 }}> => match v with
                       | <{ inl T' v0 }> =>
                           if ty_dec T' T1 then value_index T0 v0 else None
                       | <{ inr T' v1 }> => 
                           match ty_dec T' T0, value_index T1 v1 with
                           | in_left, Some n => Some (max (dim T0) (dim T1) + n)
                           | _, _ => None
                           end
                       | _ => None
                       end
  | <{{ T1 * T2 }}> => match v with
                      | <{ <v1, v2> }> =>
                          match value_index T1 v1, value_index T2 v2 with
                          | Some n1, Some n2 => Some (n1 * dim T2 + n2)
                          | _, _ => None
                          end
                      | _ => None
                      end
  end.

Lemma value_index_lt_dim :
  forall T v n, value_index T v = Some n -> n < dim T.
Proof.
  intros T.
  induction T as [| T0 IH0 T1 IH1 | T1 IH1 T2 IH2]; simpl;
  intros [] n H; try discriminate.
  - injection H as. subst. constructor.
  - destruct (ty_dec T2 T1); try discriminate. subst.
    apply IH0 in H. lia.
  - destruct (ty_dec T2 T0); try discriminate. subst.
    destruct (value_index T1 e) as [n1 |] eqn:E; try discriminate.
    injection H as. subst. apply IH1 in E. lia.
  - destruct (value_index T1 e1) as [n1 |] eqn:E1; try discriminate.
    destruct (value_index T2 e2) as [n2 |] eqn:E2; try discriminate.
    injection H as. subst. apply IH1 in E1. apply IH2 in E2.
    eapply lt_le_trans.
    + apply plus_lt_compat_l. apply E2.
    + rewrite <- Nat.mul_succ_l. apply mult_le_compat_r. lia.
Qed.

Lemma value_index_in_type :
  forall T v, value_index T v <> None <-> value_in_type v T.
Proof.
  intros T v. split.
  - generalize dependent v.
    induction T as [| T0 IH0 T1 IH1 | T1 IH1 T2 IH2]; simpl;
    intros [] H; try contradiction.
    + constructor.
    + destruct (ty_dec T2 T1); try contradiction.
      subst. auto using value_in_type.
    + destruct (ty_dec T2 T0); try contradiction.
      subst. destruct (value_index T1 e) eqn:E; try contradiction.
      constructor. apply IH1. rewrite E. discriminate.
    + destruct (value_index T1 e1) as [n1 |] eqn:E1; try contradiction.
      destruct (value_index T2 e2) as [n2 |] eqn:E2; try contradiction.
      constructor.
      * apply IH1. rewrite E1. discriminate.
      * apply IH2. rewrite E2. discriminate.
  - intros H. induction H; simpl.
    + discriminate.
    + destruct (ty_dec T1 T1); auto.
    + destruct (ty_dec T0 T0); try contradiction.
      destruct (value_index T1 e) eqn:E; try contradiction.
      discriminate.
    + destruct (value_index T1 e1) as [n1 |] eqn:E1; try contradiction.
      destruct (value_index T2 e2) as [n2 |] eqn:E2; try contradiction.
      discriminate.
Qed.

(* The value with a bit representation corresponding to the given number. *)
Fixpoint nth_value T n :=
  match T with
  | <{{ Unit }}> => match n with
                    | 0 => Some <{ <> }>
                    | S _ => None
                    end
  | <{{ T0 + T1 }}> => 
      let subspace := max (dim T0) (dim T1) in
      if n <? subspace
      then match nth_value T0 n with
           | None => None
           | Some e0 => Some <{ inl T1 e0 }>
           end
      else match nth_value T1 (n - subspace) with
           | None => None
           | Some e1 => Some <{ inr T0 e1 }>
           end
  | <{{ T1 * T2 }}> =>
      match nth_value T1 (n / dim T2),
            nth_value T2 (n mod dim T2)
      with
      | Some e1, Some e2 => Some <{ <e1, e2> }>
      | _, _ => None
      end
  end.

Lemma nth_value_index :
  forall T v n, value_index T v = Some n <-> nth_value T n = Some v.
Proof.
  intros T. induction T as [| T0 IH0 T1 IH1 | T1 IH1 T2 IH2]; simpl; intros v n.
  - split; intros H.
    + destruct v; try discriminate. injection H as. subst. reflexivity.
    + destruct n; try discriminate. injection H as. subst. reflexivity.
  - split; intros H.
    + destruct v; try discriminate.
      * destruct (ty_dec T2 T1); try discriminate. subst.
        apply IH0 in H. rewrite H.
        destruct (n <? max (dim T0) (dim T1)) eqn:E; try reflexivity.
        apply Nat.ltb_nlt in E. exfalso. apply E.
        apply lt_le_trans with (m := dim T0); try lia.
        apply value_index_lt_dim with (v := v). apply IH0. assumption.
      * destruct (ty_dec T2 T0); try discriminate.
        destruct (value_index T1 v) as [n1 |] eqn:E; try discriminate.
        injection H as. subst.
        destruct (_ <? _) eqn:El; try (apply Nat.ltb_lt in El; lia).
        rewrite minus_plus.
        apply IH1 in E. rewrite E. reflexivity.
    + destruct (n <? max (dim T0) (dim T1)) eqn:E.
      * destruct (nth_value T0 n) as [v0 |] eqn:E0; try discriminate.
        injection H as. subst.
        destruct (ty_dec T1 T1); try contradiction.
        apply IH0. assumption.
      * destruct (nth_value T1 _) as [v1 |] eqn:E1; try discriminate.
        injection H as. subst. destruct (ty_dec T0 T0); try contradiction.
        apply IH1 in E1. rewrite E1.
        rewrite <- le_plus_minus; try reflexivity.
        apply Nat.ltb_nlt in E. lia.
  - split; intros H.
    + destruct v; try discriminate.
      destruct (value_index T1 v1) as [n1 |] eqn:E1; try discriminate.
      destruct (value_index T2 v2) as [n2 |] eqn:E2; try discriminate.
      injection H as. subst.
      rewrite Nat.div_add_l; try apply dim_nonzero.
      rewrite Nat.div_small; eauto using value_index_lt_dim.
      rewrite <- plus_n_O. apply IH1 in E1. rewrite E1.
      rewrite plus_comm, Nat.mod_add; try apply dim_nonzero.
      rewrite Nat.mod_small; eauto using value_index_lt_dim.
      apply IH2 in E2. rewrite E2. reflexivity.
    + destruct (nth_value T1 (n / dim T2)) as [e1 |] eqn:E1; try discriminate.
      destruct (nth_value T2 (n mod dim T2)) as [e2 |] eqn:E2; try discriminate.
      injection H as. subst.
      apply IH1 in E1. apply IH2 in E2. rewrite E1, E2.
      rewrite Nat.mul_comm, <- Nat.div_mod; auto using dim_nonzero.
Qed.

Corollary nth_value_dim :
  forall T n, nth_value T n <> None -> n < dim T.
Proof.
  intros T n H. destruct (nth_value T n) as [v |] eqn:E; try contradiction.
  apply value_index_lt_dim with (v := v). apply nth_value_index. assumption.
Qed.

Lemma value_index_inj :
  forall T v v' n,
  value_index T v = Some n ->
  value_index T v' = Some n ->
  v = v'.
Proof.
  intros T v v' n H H'.
  apply nth_value_index in H, H'. rewrite H in H'. injection H'. auto.
Qed.

Lemma nth_value_inj :
  forall T n n' v,
  nth_value T n = Some v ->
  nth_value T n' = Some v ->
  n = n'.
Proof.
  intros T n n' v H H'.
  apply nth_value_index in H, H'. rewrite H in H'. injection H'. auto.
Qed.

(* Convert a expression-indexed matrix to a natural-indexed matrix. *)
Definition mixed2matrix T p : Density (dim T) :=
  fun i j => match nth_value T i, nth_value T j with
             | Some v_i, Some v_j => p v_i v_j
             | _, _ => C0
             end.

Lemma wf_mixed2matrix :
  forall T p, WF_Matrix (mixed2matrix T p).
Proof.
  intros T p i j H. unfold mixed2matrix.
  destruct (nth_value T i) as [v_i |] eqn:Ei; try reflexivity.
  destruct H.
  - assert (i < dim T); try lia.
    apply nth_value_dim. rewrite Ei. discriminate.
  - destruct (nth_value T j) as [v_j |] eqn:Ej; try reflexivity.
    assert (j < dim T); try lia.
    apply nth_value_dim. rewrite Ej. discriminate.
Qed.

(** A density operator corresponding to a uniform distribution over all basis
    values. *)
Definition maximally_mixed T : mixed :=
  fun e_c e_r => if expr_dec e_c e_r 
                 then if value_in_type_dec e_c T then / INR (cardinal T) else C0
                 else C0.
