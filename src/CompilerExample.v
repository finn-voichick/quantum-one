(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import Nat.
Require Import DensitySem.
Require Import QuantumOne Semantics src.Types VectorTypes Lib Compiler CoinFlip.

(* A superoperator that discards the last qubit. *)
Definition discard_last {m} : Superoperator (2^(S m)) (2^m) :=
  Splus (super (kron (I (2^m)) (bra 0))) (super (kron (I (2^m)) (bra 1))).

Lemma I_1_kron :
  forall n m (A : Matrix n m), WF_Matrix A -> kron (I 1) A = A.
Proof.
  intros n m A H. gridify.
Qed.

Lemma wf_discard_last :
  forall m, WF_Superoperator (@ discard_last m).
Proof.
  intros m. induction m.
  - intros p H. unfold discard_last. simpl in *.
Abort.

(* Discard the last n qubits. *)
Fixpoint discard_last_n {m} n : Superoperator (2^(m + n)) (2^m) :=
  match n with
  | 0 => super (I m)
  | S n' => compose_super (discard_last_n n') discard_last
  end.

(* Discard all but the first n qubits. *)
Definition keep_first_n {m} n : Superoperator (2^m) (2^n) :=
  discard_last_n (m - n).

(* Pad the state with m qubits in the 0 state. *)
Definition append_n {m} n : Superoperator (2^m) (2^(m + n)) :=
  super (kron (I m) (kron_n n (ket 0))).

(* Compute a superoperator from a SQIR program. *)
Definition circuit_eval i {dim} o (p : base_com dim) :
  Superoperator (2^i) (2^o) :=
  compose_super (compose_super (keep_first_n o) (c_eval p))
                (append_n (dim - i)).

(* Compilation is correct if semantics and circuit are equivalent. *)
Definition e_compile_correct dim T e :=
  exists p,
  e =[ p ] /\
  mixed2matrix T p =
  (@circuit_eval 0
                 dim
                 (size T)
                 (mcom2com (compile_expr e Maps.empty Maps.empty 0)))
  (I 1).

Lemma maximally_mixed2matrix :
  forall T,
  mixed2matrix T (maximally_mixed T) =
  fun i j =>
  match i =? j, nth_value T i with
  | true, Some _ => / INR (cardinal T)
  | _, _ => 0
  end.
Proof.
  intros T.
  apply functional_extensionality. intros i.
  apply functional_extensionality. intros j.
  unfold mixed2matrix, maximally_mixed, scale, I.
  destruct (nth_value T i) as [v_i |] eqn:Ei;
  try (destruct (i =? j); reflexivity).
  destruct (i =? j) eqn:E.
  - rewrite Nat.eqb_eq in E. subst. rewrite Ei.
    destruct (expr_dec v_i v_i); try contradiction.
    destruct (value_in_type_dec v_i T); try reflexivity.
    exfalso. apply n. apply value_index_in_type.
    apply nth_value_index in Ei. rewrite Ei. discriminate.
  - destruct (nth_value T j) as [v_j |] eqn:Ej; try reflexivity.
    destruct (expr_dec v_i v_j); try reflexivity.
    subst. exfalso. apply Nat.eqb_neq in E. apply E.
    eapply nth_value_inj.
    + apply Ei.
    + apply Ej.
Qed.

Lemma coin_flip_compile_correct :
  e_compile_correct 4 <{{ Bit }}> coin_flip.
Proof.
  exists (maximally_mixed <{{ Bit }}>).
  split; try apply coin_flip_maximally_mixed.
  unfold coin_flip.
  rewrite maximally_mixed2matrix.
  apply functional_extensionality. intros i.
  apply functional_extensionality. intros j.
Abort.
