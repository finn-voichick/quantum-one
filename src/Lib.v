(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import String Reals.
Require Import Maps.
Require Import QuantumOne src.Types.
Import VarNames.

Definition id T := <{ \x : T, x }>.

Lemma has_type_id :
  forall T,
  |> {id T} : (T -> T).
Proof. type_check. Qed.

Definition const T e := <{ \x : T, e }>.

Lemma has_type_const :
  forall e T' T,
  |- e : T ->
  |> {const T' e} : (T' -> T).
Proof.
  unfold const. auto with type_db.
Qed.

Definition not := U_R PI 0 PI.

Definition hadamard := U_R (PI / 2) 0 PI.

Definition cnot :=
  <{ \x : Bit * Bit,
     let <x1, x2> = x in control x1 with | 0 => x2 | 1 => not x2 }>.

Lemma has_type_cnot :
  |> cnot : (Bit * Bit -> Bit * Bit).
Proof. type_check. Qed.

Definition meas T := <{ \x : T, ((\y : T, x) x) }>.

Lemma has_type_meas :
  forall T,
  |> {meas T} : (T -> T).
Proof.
  intros T. repeat econstructor.
Qed.

Definition fst T1 T2 := <{ \x : T1 * T2, let <x1, x2> = x in x1 }>.

Lemma has_type_fst :
  forall T1 T2,
  |> {fst T1 T2} : (T1 * T2 -> T1).
Proof. type_check. Qed.

Definition snd T1 T2 := <{ \x : T1 * T2, let <x1, x2> = x in x2 }>.

Lemma has_type_snd :
  forall T1 T2,
  |> {snd T1 T2} : (T1 * T2 -> T2).
Proof. type_check. Qed.

Definition plus := <{ hadamard 0 }>.

Lemma has_type_plus :
  |- plus : Bit.
Proof. type_check. Qed.

Definition minus := <{ hadamard 1 }>.

Lemma has_type_minus :
  |- minus : Bit.
Proof. type_check. Qed.

#[export] Hint Resolve has_type_id has_type_const has_type_cnot has_type_meas
                       has_type_fst has_type_snd has_type_plus has_type_minus :
  type_db.

Fixpoint qmap T f n :=
  match n with
  | 0 => id <{{ Unit }}>
  | S n' => <{ \x : Array<T>[n], let <x0, x'> = x in <f x0, {qmap T f n'} x'> }>
  end.

Lemma has_type_qmap :
  forall T T' f n,
  |> f : (T -> T') ->
  |> {qmap T f n} : (Array<T>[n] -> Array<T'>[n]).
Proof.
  intros T T' f n H. induction n as [| n]; simpl; auto with type_db.
  constructor. econstructor.
  - discriminate.
  - constructor. reflexivity.
  - eauto 8 with type_db.
Qed.

Fixpoint replicate n T :=
  match n with
  | 0 => const T <{ <> }>
  | S n' => <{ \x : T, <x, {replicate n' T} x> }>
  end.

Lemma has_type_replicate :
  forall n T,
  |> {replicate n T} : (T -> Array<T>[n]).
Proof.
  intros n T. induction n; simpl; eauto 7 with type_db.
Qed.

#[export] Hint Resolve has_type_qmap has_type_replicate : type_db.

Definition multi_hadamard n := qmap <{{ Bit }}> hadamard n.

Lemma has_type_multi_hadamard :
  forall n,
 |> {multi_hadamard n} : (Reg[n] -> Reg[n]).
Proof. eauto with type_db. Qed.

Fixpoint all T p n :=
  match n with
  | 0 => const <{{ Unit }}> <{ 1 }>
  | S n' => <{ \x : Array<T>[n], let <x0, x'> = x in case p x0 of
                                                     | 0 => 0
                                                     | 1 => {all T p n'} x' }>
  end.

Lemma has_type_all :
  forall T p n,
  |> p : (T -> Bit) ->
  |> {all T p n} : (Array<T>[n] -> Bit).
Proof.
  intros T p n H. induction n as [| n]; simpl; auto with type_db.
  constructor. econstructor.
  - discriminate.
  - constructor. reflexivity.
  - constructor; eauto with type_db.
Qed.

#[export] Hint Resolve has_type_multi_hadamard has_type_all : type_db.
