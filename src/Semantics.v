(* QuantumOne Language - a simple functional quantum programming language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of QuantumOne Language.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import RelationClasses.
Require Import Maps.
Require Import Complex.
Require Import QuantumOne src.Vector.

Open Scope R_scope.
Open Scope C_scope.

(** Functions are first-order, so the only values are pure data. *)
Inductive value : expr -> Prop :=
  | value_unit :
      value <{ <> }>
  | value_inl T v :
      value v ->
      value <{ inl T v }>
  | value_inr T v :
      value v ->
      value <{ inr T v }>
  | value_pair v1 v2 :
      value v1 ->
      value v2 ->
      value <{ <v1, v2> }>.

Definition value_dec :
  forall e, {value e} + {~ value e}.
Proof.
  intros e. induction e; try now (right; intros H; inversion H).
  - auto using value.
  - destruct IHe; auto using value.
    right. intros H. inversion H. contradiction.
  - destruct IHe; auto using value.
    right. intros H. inversion H. contradiction.
  - inversion IHe1; inversion IHe2; auto using value;
    right; intros H'; inversion H'; contradiction.
Defined.

Reserved Notation "[ x := v ] e"
  (in custom q1 at level 20, x constr, right associativity).
Fixpoint subst (x : string) (v e : expr) : expr :=
  match e with
  | <{ <> }> => e
  | <{ inl T e' }> => <{ inl T ([x:=v] e') }>
  | <{ inr T e' }> => <{ inr T ([x:=v] e') }>
  | <{ <e1, e2> }> => <{ < [x:=v] e1, [x:=v] e2> }>
  | expr_var x' => if eqb_string x x' then v else e
  | <{ let <x1, x2> = e1 in e2 }> =>
      let e2' := if eqb_string x x1 || eqb_string x x2
                 then e2
                 else <{ [x:=v] e2 }> in
      <{ let <x1, x2> = [x:=v] e1 in e2' }>
  | <{ case e' of | inl x0 => e0 | inr x1 => e1 }> =>
      let e0' := if eqb_string x x0 then e0 else <{ [x:=v] e0 }> in
      let e1' := if eqb_string x x1 then e1 else <{ [x:=v] e1 }> in
      <{ case [x:=v] e' of | inl x0 => e0' | inr x1 => e1' }>
  | <{ case e' of | 0 => e0 | 1 => e1 }> =>
      <{ case [x:=v] e' of | 0 => [x:=v] e0 | 1 => [x:=v] e1 }>
  | <{ control e' with | inl x0 => e0 | inr x1 => e1 }> =>
      let e0' := if eqb_string x x0 then e0 else <{ [x:=v] e0 }> in
      let e1' := if eqb_string x x1 then e1 else <{ [x:=v] e1 }> in
      <{ case [x:=v] e' of | inl x0 => e0' | inr x1 => e1' }>
  | <{ control e' with | 0 => e0 | 1 => e1 }> =>
      <{ case [x:=v] e' of | 0 => [x:=v] e0 | 1 => [x:=v] e1 }>
  | <{ f e1 }> =>
      let e1' := <{ [x:=v] e1 }> in
      match f with
      | <{ \x' : T, e2 }> =>
          if eqb_string x x' then <{ f e1' }> else <{ (\x' : T, [x:=v] e2) e1' }>
      | fn_gate g => <{ g e1' }>
      end
  end
where "[ x := v ] e" := (subst x v e) (in custom q1).

Inductive red_context : Set :=
  | ctx_hole
  | ctx_inl (T : ty) (c : red_context)
  | ctx_inr (T : ty) (c : red_context)
  | ctx_pair1 (c : red_context) (e : expr)
  | ctx_pair2 (v : expr) (c : red_context) : value v -> red_context
  | ctx_case (c : red_context)
             (x0 : string)
             (e0 : expr)
             (x1 : string)
             (e1 : expr)
  | ctx_if (c : red_context) (e0 : expr) (e1 : expr)
  | ctx_ctrl (c : red_context)
             (x0 : string)
             (e0 : expr)
             (x1 : string)
             (e1 : expr)
  | ctx_ctrl_bit (c : red_context) (e0 : expr) (e1 : expr)
  | ctx_let (x1 x2 : string) (c : red_context) (e : expr)
  | ctx_app (f : fn) (c : red_context).

Fixpoint plug (c : red_context) (e : expr) :=
  match c with
  | ctx_hole => e
  | ctx_inl T c' => <{ inl T {plug c' e} }>
  | ctx_inr T c' => <{ inr T {plug c' e} }>
  | ctx_pair1 c' e' => <{ < {plug c' e}, e'> }>
  | ctx_pair2 v c' H => <{ <v, {plug c' e} > }>
  | ctx_case c' x0 e0 x1 e1 =>
      <{ case {plug c' e} of | inl x0 => e0 | inr x1 => e1 }>
  | ctx_if c' e0 e1 => <{ case {plug c' e} of | 0 => e0 | 1 => e1 }>
  | ctx_ctrl c' x0 e0 x1 e1 =>
      <{ control {plug c' e} with | inl x0 => e0 | inr x1 => e1 }>
  | ctx_ctrl_bit c' e0 e1 =>
      <{ control {plug c' e} with | 0 => e0 | 1 => e1 }>
  | ctx_let x1 x2 c' e' => <{ let <x1, x2> = {plug c' e} in e' }>
  | ctx_app f c' => <{ f {plug c' e} }>
  end.

(*
Fixpoint next_context e :=
  match e with
  | <{ <> }> => None
  | <{ inl e1 }> => match next_context e1 with
                    | None => None
                    | Some (c, e') => Some (ctx_inl c, e')
                    end
  | <{ inr e1 }> => match next_context e1 with
                    | None => None
                    | Some (c, e') => Some (ctx_inr c, e')
                    end
  | <{ <e1, e2> }> =>
      match value_dec e1 with
      | left P => match next_context e2 with
                  | None => None
                  | Some (c, e') => Some (ctx_pair2 e1 c P, e')
                  end
      | right _ => match next_context e1 with
                   | None => None
                   | Some (c, e') => Some (ctx_pair1 c e2, e')
                   end
      end
  | expr_var x => None
  end.
*)

Definition state := vector (expr * list expr).

Definition vmap_expr f : state -> state :=
  vmap (fun x : expr * list expr => let (e, d) := x in (f e, d)).

Reserved Notation "e ~~> s" (at level 40).
Inductive cstep :  expr -> state -> Prop :=
  | cstep_app_abs1 x T e v : 
      value v ->
      appears_free_in x e ->
      <{ (\x : T, e) v }> ~~> ket (<{ [x:=v] e }>, [])
  | cstep_app_abs2 x T e v : 
      value v ->
      ~ appears_free_in x e ->
      <{ (\x : T, e) v }> ~~> ket (e, v :: [])
  | step_let_pair1 x1 x2 v1 v2 e :
      value v1 ->
      value v2 ->
      appears_free_in x1 e ->
      appears_free_in x2 e ->
      <{ let <x1, x2> = <v1, v2> in e }> ~~> ket (<{ [x1:=v1] [x2:=v2] e }>, [])
  | step_let_pair2 x1 x2 v1 v2 e :
      value v1 ->
      value v2 ->
      appears_free_in x1 e ->
      ~ appears_free_in x2 e ->
      <{ let <x1, x2> = <v1, v2> in e }> ~~> ket (<{ [x1:=v1] e }>, v2 :: [])
  | step_let_pair3 x1 x2 v1 v2 e :
      value v1 ->
      value v2 ->
      ~ appears_free_in x1 e ->
      appears_free_in x2 e ->
      <{ let <x1, x2> = <v1, v2> in e }> ~~> ket (<{ [x2:=v2] e }>, v1 :: [])
  | step_let_pair4 x1 x2 v1 v2 e :
      value v1 ->
      value v2 ->
      ~ appears_free_in x1 e ->
      ~ appears_free_in x2 e ->
      <{ let <x1, x2> = <v1, v2> in e }> ~~> ket (e, v1 :: v2 :: [])
  | step_case_inl1 T v x0 e0 x1 e1 :
      value v ->
      appears_free_in x0 e0 ->
      <{ case inl T v of | inl x0 => e0 | inr x1 => e1 }> ~~>
      ket (<{ [x0:=v] e0 }>, <{ 0 }> :: [])
  | step_case_inl2 T v x0 e0 x1 e1 :
      value v ->
      ~ appears_free_in x0 e0 ->
      <{ case inl T v of | inl x0 => e0 | inr x1 => e1 }> ~~>
      ket (e0, <{ inl T v }> :: [])
  | step_case_inr1 T v x0 e0 x1 e1 :
      value v ->
      appears_free_in x1 e1 ->
      <{ case inr T v of | inl x0 => e0 | inr x1 => e1 }> ~~>
      ket (<{ [x1:=v] e1 }>, <{ 1 }> :: [])
  | step_case_inr2 T v x0 e0 x1 e1 :
      value v ->
      ~ appears_free_in x1 e1 ->
      <{ case inl T v of | inl x0 => e0 | inr x1 => e1 }> ~~>
      ket (e0, <{ inr T v }> :: [])
  | step_if0 e0 e1 :
      <{ case 0 of | 0 => e0 | 1 => e1 }> ~~> ket (e0, <{ 0 }> :: [])
  | step_if1 e0 e1 :
      <{ case 1 of | 0 => e0 | 1 => e1 }> ~~> ket (e1, <{ 1 }> :: [])
  | step_ctrl_inl T v x0 e0 x1 e1 :
      value v ->
      <{ control inl T v with | inl x0 => e0 | inr x1 => e1 }> ~~>
      ket (<{ <inl T v, [x0:=v] e0> }>, [])
  | step_ctrl_inr T v x0 e0 x1 e1 :
      value v ->
      <{ control inr T v with | inl x0 => e0 | inr x1 => e1 }> ~~>
      ket (<{ <inr T v, [x1:=v] e1> }>, [])
  | step_ctrl0 e0 e1 :
      <{ control 0 with | 0 => e0 | 1 => e1 }> ~~> ket (<{ <0, e0> }>, [])
  | step_ctrl1 e0 e1 :
      <{ control 1 with | 0 => e0 | 1 => e1 }> ~~> ket (<{ <1, e1> }>, [])
  | step_gate0 theta phi lambda :
      <{ {U_R theta phi lambda} 0 }> ~~>
      (vector_sum (vector_scale (cos (theta / 2)) (<{ 0 }>, []))
                  (vector_scale (Cexp phi * sin (theta / 2))
                                (<{ 1 }>, [])))
  | step_gate1 theta phi lambda :
      <{ {U_R theta phi lambda} 0 }> ~~>
      (vector_sum (vector_scale (- Cexp lambda * sin (theta / 2))
                              (<{ 0 }>, []))
                  (vector_scale (Cexp (lambda + phi) * cos (theta / 2))
                              (<{ 1 }>, [])))
where "e ~~> s" := (cstep e s).

(** vvalue just means that a state is a fully reduced vector of values. *)
Inductive vvalue : state -> Prop :=
  | vvalue_scale alpha e d :
      value e ->
      vvalue (vector_scale alpha (e, d))
  | vvalue_sum v0 v1 :
      vvalue v0 ->
      vvalue v1 ->
      vvalue (vector_sum v0 v1).

(** This is the "actual" step relation, stepping quantum states to quantum
    states. It is based on sstep. *)
Reserved Notation "s --> s'" (at level 40).
Inductive step : state -> state -> Prop :=
  | step_scale alpha e d s c :
      e ~~> s ->
      vector_scale alpha (plug c e, d) -->
      scale alpha
            (vmap (fun x : expr * list expr =>
                   let (e', d') := x in (plug c e', d' ++ d)) s)
  | step_sum0 s0 s0' s1 :
      s0 --> s0' ->
      vector_sum s0 s1 --> vector_sum s0' s1
  | step_sum1 v s s' :
      vvalue v ->
      s --> s' ->
      vector_sum v s --> vector_sum v s'
where "s --> s'" := (step s s').

Reserved Notation "v '-->*' v'" (at level 40).
Inductive multistep : state -> state -> Prop :=
  | multi_refl v : v -->* v
  | multi_step v1 v2 v3 :
      v1 --> v2 ->
      v2 -->* v3 ->
      v1 -->* v3
where "v '-->*' v'" := (multistep v v').

Instance multistep_refl :
  Reflexive multistep.
Proof.
  intros x. apply multi_refl.
Qed.

Definition mixed := density expr.

(** Discards the discard list of the state, producing a density operator. *)
Definition collapse (v : state) : mixed :=
  fun e_c e_r =>
  vsum (fun alpha_i (p_i : expr * list expr) =>
        let (e_i, e_i') := p_i in
        if expr_dec e_r e_i
        then alpha_i *
        vsum (fun alpha_j (p_j : expr * list expr) =>
              let (e_j, e_j') := p_j in
              if expr_dec e_c e_j
              then if list_eq_dec expr_dec e_i' e_j' then alpha_j^* else C0
              else C0)
             v
        else C0)
       v.

(** This definition shows how an expression can be "represented" by a given
    density operator. *)
Definition has_density e r :=
  exists v, vvalue v /\ ket (e, []) -->* v /\ collapse v = r.

Notation "e =[ r ]" := (has_density e r) (at level 50).

